function testResults = exampleTest
testResults = mautogradeFunctionRunTests(localfunctions);
end

function score=testFunctionFailScore(testCase)
% DESCRIPTION = This test should fail
assert(0==1)
score=1;
end

function [score,output]=testFunctionPassScoreOverMax(testCase)
% DESCRIPTION = This test should pass, but mAutograde should give a warning about the score being over the maximum
assert(1==1)
score=2;
output='';
end

function score=testFunctionFailMaxScore(testCase)
% MAX_SCORE = 2
% DESCRIPTION = This test should fail
assert(0==1)
score=2;
end

function score=testFunctionPassMaxScore(testCase)
% MAX_SCORE = 2
score=2;
end

function score=testFunctionPassMaxScoreWithNormalization(testCase)
% MAX_SCORE = 0.5
% MAX_SCORE_BEFORE_NORMALIZATION = 2
% DESCRIPTION = Shows how to use the normalization option
score=2;
end
