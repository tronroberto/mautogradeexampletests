function testResults = testFile
testResults = mautogradeFunctionRunTests(localfunctions);
end

function FunctionNoScorePass(testCase)
assert(1==1)
end

function FunctionNoScoreFail(testCase)
% DESCRIPTION = This test should fail
assert(0==1)
end

function FunctionNoScoreMaxScorePass(testCase)
% MAX_SCORE = 3
assert(1==1)
end

function FunctionNoScoreMaxScoreFail(testCase)
% MAX_SCORE = 3
% DESCRIPTION = This test should fail
assert(0==1)
end
