function testResults = function2_autoTest
testResults = mautogradeFunctionRunTests(localfunctions);
end
function [score,output]=hinter(testCase)
% MAX_SCORE=0.3
% MAX_SCORE_BEFORE_NORMALIZATION=1
[score,output]=mautogradeTestStyleHinter(testCase.functionTestedFileName);
  output=mautogradeOutputAppend('Note: With the example solution, this test should NOT pass with full score',output);
  output=mautogradeOutputAppend('This illustrates on of the more opinionated hinter suggestions.',output);
end
function [score,output]=types(testCase)
  % MAX_SCORE=0.1
  % MAX_SCORE_BEFORE_NORMALIZATION=6
  % The data for testing and the maximum score before normalization are updated
  % automatically by the function1_autoTestData.m script
  % The description for this test will be generated automatically
  load([testCase.functionTestedStr '_autoTestData.mat'],'dataInOutTypes')
  [score,output]=mautogradeTestInOutTypes(testCase.functionTested,dataInOutTypes);
end
function [score,output]=dimensions(testCase)
  % MAX_SCORE=0.1
  % MAX_SCORE_BEFORE_NORMALIZATION = 6
  % The data for testing and the maximum score before normalization are updated
  % automatically by the function1_autoTestData.m script
  load([testCase.functionTestedStr '_autoTestData.mat'],'dataInOutDimensions')
  [score,output]=mautogradeTestInOutDimensions(testCase.functionTested,dataInOutDimensions);
end
function [score,output]=inOut(testCase)
  % MAX_SCORE_BEFORE_NORMALIZATION = 6
  % The data for testing and the maximum score before normalization are updated
  % automatically by the function1_autoTestData.m script
  % The description for this test will be generated automatically
  load([testCase.functionTestedStr '_autoTestData.mat'],'dataInOut')
  [score,output]=mautogradeTestInOut(testCase.functionTested,dataInOut);
end
