% [fractionCorrect,totalItems]=cmpUnordered(expected, actual)
% Function for comparing two arrays after sorting so that the
% order of their contents does not matter
function [fractionCorrect,totalItems]=cmpUnordered(expected, actual)
% A robust implementation is to simply sort and then use mautogradeCmpEq
% See mautogradeCmpEq for details on the meaning of the output arguments
[fractionCorrect,totalItems]=mautogradeCmpEq(sort(expected(:)), sort(actual(:)));
