# Overview

Example tests that can be checked out and run with mAutograde
(<https://bitbucket.org/tronroberto/mautograde/>)

# Instructions

-   Test files should start or end with `test` (case insensitive), and
    have extension `.m`
-   The main function of each test file should follow this example

``` matlab
function testResults = exampleTest
testResults = mautogradeFunctionTests(localfunctions);
end
```

-   Every other local function is treated as a test.
-   The current version of mAutograde does not support test fixtures
    (setup/tear down)

# Writing tests

## Basic tests

The most simple test has the following form:

``` matlab
function FunctionNoScorePass(testCase)
assert(1==1)
end
```

If the assertion is not valid, an exception is raised, making the test
fail. Any other error will also make the test fail. See `testFile.m` for
other similar examples.

## Tests returning scores

Each function can optionally return a score. This allows one to write
tests that can partially pass or fail, such as the following:

function score=testFunctionFailScore(testCase) assert(0==1) score=1; end

See `exampleTest.m` for other similar examples.

## Tests returning scores and custom outputs

A second optional return argument is interpreted as a string to be
included in the result shown on Gradescope (independently on whether the
test passes or fails). See `function1_autoTest.m` on how to append
custom messages to the output.

## Standard tests

There are four test functions that are included in all the examples for
this tutorial.

### Hinter tests (function `hinter`)

Runs the script `matlabStyleHinter.m`: this is an hinter, included in
`mAutograder`, that runs the Matlab hinter, plus additional checks (see
`help matlabStyleHinter` for additional details). This hinter is quite
opinionated and a little pedantic, but encourages the student to write
more readable code. The suggested MAX~SCORE~ for this test is 0.3.

### Tests for output sizes and types (functions `types` and `dimensions`)

The tests check if the outputs of the function have the expected sizes
and the right types by using the provided functions
`mautogradeTestInOutDimensions` and `mautogradeTestInOutTypes`. While
these tests are rather trivial, they are very useful for assignments
that build on previous assignments; in particular, with these checks in
place, the students will have less problems dropping in past assignment
solutions, and there will also be less problems in debugging problems
with the students\' submissions. The suggested MAX~SCORE~ for each of
these tests is 0.1.

### Tests for input/output pairs generated from solutions

These tests require setting up a solution first. Please see the
`TUTORIAL.org` file in the main `mAutograde` repository for details.

## Tagging tests with options

In each test, lines that follow the `function` statement for each test,
and that have the format

``` matlab
% OPTION_NAME = value
```

are interpreted as setting an option for that particular test; see below
for supported options.

It is important that lines with options follow the `function` statement
line **immediately**, otherwise they will not be recognized by the
parser. Options that are not included in the list below are treated as
simple Matlab comments. See the various test files for examples of tests
with options.

### MAX~SCORE~

Sets the maximum obtainable score. The default is the maximum between 1
and the actual score passed by the test (if returned).

### MAX~SCOREBEFORENORMALIZATION~

Sets the maximum score returned by the test. The system will divide the
actual score returned by the test by the value of
`MAX_SCORE_BEFORE_NORMALIZATION` and multiply it by `MAX_SCORE`.

### VISIBILITY

Sets the visibility of the test output. Options recognized by Gradescope
are:

-   `visible` (default): test case will always be shown.
-   `hidden`: test case will never be shown to students.
-   `after_due_date`: test case will be shown after the assignment\'s
    due date has passed.
-   `after_published`: test case will be shown only when the assignment
    is explicitly published from the \"Review Grades\" page.

See the official documentation at
<https://gradescope-autograders.readthedocs.io/en/latest/specs/> for
further details.

### DESCRIPTION

Gives a description of the test, which is then included in the output
(independently on whether the test passed or not). For some functions, a
default description is generated if one is not provided (see
`mautogradeFunctionDescriptionDefault`).

## Checking for equality

You can check if two variables are equal using the `mautogradeCmpEq`
function. This function will work on arrays, cell arrays, structs and
strings. For numerical values, you can include add an optional tolerance
parameter (giving a tolerance for non-numeric expected values will
generate an error).

## Information provided in testCase for every test

The argument `testCase` passed to each function is a struct with fields:

-   `functionTested`: handle to the function under test, obtained from
    the file name of the suite. E.g., if the suite is saved in the file
    `sin_autoTest.m`, then `testCase.functionTested=@sin`.
    **Octave-specific:** Octave cannot create handles to functions that
    are not defined, or to functions that contain nested functions (even
    if the function referred to is not a nested function itself). In
    these cases, `functionTested` will contain the name of the function
    as a `char` array. Note that `mautogradeTestInOut` accepts such
    string representation.
-   `functionTestedStr`: string representation of the `functionTested`
    field.

If the test suite file does not follow the convention mentioned above,
the fields `functionTested` and `functionTestedStr` will not be
available.

`.functionTestedFileName`
:   The name of the file with the function being tested.
